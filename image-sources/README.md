If you're using this as a template to start your Playdate project, you can just
discard this directory. It consists of images I used to create the SystemAssets
graphics and the template's icon for use at GitLab and similar sites.

The picture of a penguin using a computer is from
https://openclipart.org/detail/132427/penguin-admin by Moini and was released
into the public domain.
