Template Game for Playdate SDK on Linux
=======================================

This is a template for [Playdate]<https://play.date/> software development
under Linux. It may work with some changes under MacOS.

Requirements
-------------

- [GNU Make]<https://www.gnu.org/software/make/>
- A POSIX-like environment, including find, grep, head, printf, rm, sed, and tr
- The environment variable `PLAYDATE_SDK_PATH` is correctly set
- The Playdate SDK binaries are in `$PLAYDATE_SDK_PATH/bin`

Setup
-----

1. Make a clone or copy.
2. Edit `src/pdxinfo` with your game's information.
3. Replace `SystemAssets/*.png` with your own graphics.


Usage
-----

- `make` to create your PDX.
- `make run` to launch the simulator.
- `make clean` to remove the created PDX.
- Put additional source and assets into `src/`.


License and Copyright
---------------------

This was created by Alan De Smet, and is based on the "Game Template" example
from the Playdate SDK. There is negligable creative content here. To the extent
that I might be able to claim copyright on the images, the Makefile, and this
README, I, Alan De Smet waive all copyright and related or neighboring rights
to Template Game for Playdate SDK on Linux.
