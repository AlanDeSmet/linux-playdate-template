local gfx = playdate.graphics


local last = 0
local spinner = {'⬆️', '➡️', '⬇️', '⬅️'}

playdate.display.setRefreshRate(5)

gfx.drawText("This is a template Playdate program.", 20, 20)
gfx.drawText("It doesn't really do anything.", 20, 50)
gfx.drawText("But there is a simple little animation", 20, 80)
gfx.drawText("so you know it's working", 20, 100)

function playdate.update()
	gfx.drawText(spinner[last+1], 195, 145)
	last = (last+1) % 4
end
